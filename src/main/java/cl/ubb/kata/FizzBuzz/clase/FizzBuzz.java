package cl.ubb.kata.FizzBuzz.clase;

import static org.junit.Assert.*;

import org.junit.Test;

public class FizzBuzz {
	String devolver;

	public String juegoRecibeNumero(int x){
		if(x==1){
			devolver="1";
		}
		if(x==2){
			devolver="2";
		}
		if(x==3){
			devolver="Fizz";
		}
		if(x==4){
			devolver="4";
		}
		if(x==5){
			devolver="Buzz";
		}
		if(x==15){
			devolver="FizzBuzz";
		}
		return devolver;
		
	}

}
