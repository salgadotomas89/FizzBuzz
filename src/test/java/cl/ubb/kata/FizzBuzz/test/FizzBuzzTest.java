package cl.ubb.kata.FizzBuzz.test;

import static org.junit.Assert.*;

import org.junit.Test;

import cl.ubb.kata.FizzBuzz.clase.FizzBuzz;

public class FizzBuzzTest {

	@Test
	public void juegoRecibe1Devuelve1() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(1);
		//assert
		assertEquals("1",resultado);
	}
	@Test
	public void juegoRecibe2Devuelve2() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(2);
		//assert
		assertEquals("2",resultado);
	}
	@Test
	public void juegoRecibe3DevuelveFizz() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(3);
		//assert
		assertEquals("Fizz",resultado);
	}
	@Test
	public void juegoRecibe4Devuelve4() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(4);
		//assert
		assertEquals("4",resultado);
	}
	@Test
	public void juegoRecibe5DevuelveBuzz() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(5);
		//assert
		assertEquals("Buzz",resultado);
	}
	@Test
	public void juegoRecibe15DevuelveFizzBuzz() {
		//arrange
		FizzBuzz f = new FizzBuzz();
		//act
		String resultado = f.juegoRecibeNumero(15);
		//assert
		assertEquals("FizzBuzz",resultado);
	}

}
